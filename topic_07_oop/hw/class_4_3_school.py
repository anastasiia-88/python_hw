"""
Класс School.

Поля:
список людей в школе (общий list для Pupil и Worker): people,
номер школы: number.

Методы:
get_avg_mark: вернуть средний балл всех учеников школы
get_avg_salary: вернуть среднюю зп работников школы
get_worker_count: вернуть сколько всего работников в школе
get_pupil_count: вернуть сколько всего учеников в школе
get_pupil_names: вернуть все имена учеников (с повторами, если есть)
get_unique_worker_positions: вернуть список всех должностей работников (без повторов, подсказка: set)
get_max_pupil_age: вернуть возраст самого старшего ученика
get_min_worker_salary: вернуть самую маленькую зп работника
get_min_salary_worker_names: вернуть список имен работников с самой маленькой зп
(список из одного или нескольких элементов)
"""

from topic_07_oop.hw.class_4_1_pupil import Pupil
from topic_07_oop.hw.class_4_2_worker import Worker


class School:
    def __init__(self, people: list, number: int):
        self.people = people
        self.number = number

    def get_pupils(self):
        return [i for i in self.people if isinstance(i, Pupil)]

    def get_workers(self):
        return [i for i in self.people if isinstance(i, Worker)]

    def get_worker_count(self):
        return len(self.get_workers())

    def get_pupil_count(self):
        return len(self.get_pupils())

    def get_pupil_names(self):
        return [i.name for i in self.get_pupils()]

    def get_max_pupil_age(self):
        return max([i.age for i in self.get_pupils()])

    def get_min_worker_salary(self):
        return min([i.salary for i in self.get_workers()])

    def get_min_salary_worker_names(self):
        min_salary = self.get_min_worker_salary()
        return [i.name for i in self.get_workers() if i.salary == min_salary]

    def get_unique_worker_positions(self):
        return set([i.position for i in self.get_workers()])

    def get_avg_mark(self):
        a = [i.get_avg_mark() for i in self.get_pupils()]
        return sum(a) / len(a)

    def get_avg_salary(self):
        b = [i.salary for i in self.get_workers()]
        return sum(b) / len(b)
