from enum import Enum, auto


class Characters(Enum):
    Boss = auto()
    Maniac = auto()
    Werewolf = auto()
    Cultist = auto()
    Leader = auto()
    Avenger = auto()
    Detective = auto()
    Patrolman = auto()
    Doctor = auto()
    Lucky = auto()
    Psychic = auto()
