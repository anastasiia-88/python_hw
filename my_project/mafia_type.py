from enum import Enum, auto


class Mafiatype(Enum):
    EVIL = auto()
    GOOD = auto()
    PEACEFUL = auto()

    @classmethod
    def min_value(cls):
        return cls.PEACEFUL.value

    @classmethod
    def max_value(cls):
        return cls.EVIL.value
