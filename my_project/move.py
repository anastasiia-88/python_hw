from enum import Enum, auto

class Move(Enum):
    HIDE = auto()
    ATTACK = auto()