from my_project.mafia_type import Mafiatype

mafia_by_type = {
    Mafiatype.EVIL: {"Boss": "boss.jpg",
                     "Maniac": "maniac.jpg",
                     "Werewolf": "werewolf.jpg",
                     "Cultist": "cultist.jpg",
                     "Leader": "leader.jpg"},
    Mafiatype.GOOD: {"Avenger": "Avenger.jpg",
                     "Detective": "Detective.jpg",
                     "Patrolman": "Patrolman.jpg",
                     "Doctor": "Doctor.jpg"},
    Mafiatype.PEACEFUL: {"Lucky": "Lucky.jpg",
                         "Psychic": "Psychic.jpg"}
}
