"""
Функция print_student_marks_by_subject.

Принимает 2 аргумента: имя студента и именованные аргументы с оценками по различным предметам (**kwargs).

Выводит на экран имя студента, а затем предмет и оценку (может быть несколько, если курс состоял из нескольких частей).

Пример вызова функции print_student_marks_by_subject("Вася", math=5, biology=(3, 4), magic=(4, 5, 5)).
"""


def print_student_marks_by_subject(name, **marks):
    print(f"{name}")
    for subject,mark in marks.items():
        print(f'{subject}={mark}')


print_student_marks_by_subject("Наaaaaaaaстя", math=[5, 5], physics=[5, 4, 5], russian="2")
