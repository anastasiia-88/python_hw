"""
Функция flower_with_default_vals.

Принимает 3 аргумента:
цветок (по умолчанию "ромашка"),
цвет (по умолчанию "белый"),
цена (по умолчанию 10.25).

Функция flower_with_default_vals выводит строку в формате
"Цветок: <цветок> | Цвет: <цвет> | Цена: <цена>".

При этом в функции flower_with_default_vals есть проверка на то, что цветок и цвет - это строки

(* Подсказка: type(x) == str или isinstance(s, str)), а также цена - это число больше 0, но меньше 10000.
В функции main вызвать функцию flower_with_default_vals различными способами
(перебрать варианты: цветок, цвет, цена, цветок цвет, цветок цена, цвет цена, цветок цвет цена).

(* Использовать именованные аргументы).
"""


def flower_with_default_vals(flower_="ромашка", price_=10.25, color_="белый"):
    if type(flower_) != str or type(color_) != str:
        print('Flower and color must be string!')
    if not (0 < price_ < 10000):
        print('Price must be more than 0 and less than 10 000!')
    print(f"Цветок: {flower_} | Цена: {price_} | Цвет:{color_}")


if __name__ == '__main__':
    flower_with_default_vals(flower_='Ромашка', price_=10.25, color_='белый')

    flower_with_default_vals(flower_='Тюльпан', price_=12.25)
    flower_with_default_vals(flower_='Тюльпан', color_='черный')
    flower_with_default_vals(price_=13.25, color_='желтый')

    flower_with_default_vals(color_='оранжевый')
    flower_with_default_vals(flower_='нарцисс')
    flower_with_default_vals(price_=15.25)
