"""
Функция print_symbols_if.

Принимает строку.

Если строка нулевой длины, то вывести строку "Empty string!".

Если длина строки больше 5, то вывести первые три символа и последние три символа.
Пример: string='123456789' => result='123789'

Иначе вывести первый символ столько раз, какова длина строки.
Пример: string='345' => result='333'
"""


def print_symbols_if(s):
    s = str(s)
    a = len(s)
    if a == 0:
        print('Empty string!')
    elif a <= 5:
        n = s[0]
        print(a * n)
    elif a > 5:
        s = int(s)
        c = s % 1000
        s = str(s)
        k = s[0:3]
        print(k, c, sep="")


if __name__ == '__main__':
    print_symbols_if('')

