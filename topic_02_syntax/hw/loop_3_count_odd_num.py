"""
Функция count_odd_num.

Принимает натуральное число (целое число > 0).
Верните количество нечетных цифр в этом числе.
Если число меньше или равно 0, то вернуть "Must be > 0!".
Если число не целое (не int, а другой тип данных), то вернуть "Must be int!".
"""


def count_odd_num(a):
    odd = 0
    if type(a) != int:
        return "Must be int!"
    elif a <= 0:
        return "Must be > 0!"

    else:
        while a > 0:
            if a % 2 != 0:
                odd = odd + 1
            a = a//10
        return odd


if __name__ == '__main__':
    result = count_odd_num(345)
    print(result)
