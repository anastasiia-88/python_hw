"""
Функция if_3_do.

Принимает число.
Если оно больше 3, то увеличить число на 10, иначе уменьшить на 10.
Вернуть результат.
"""


def if_3_do(a):
    if a > 3:
        a = a + 10
        return a
    else:
        a = a - 10
        return a


if __name__ == '__main__':
    result = if_3_do(4)
    print(result)
