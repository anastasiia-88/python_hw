"""
Функция sum_1_112_3.

Вернуть сумму 1+4+7+10+...109+112.
"""


def sum_1_112_3(a, b, c):
    acc = range(a, b, c)
    return sum(acc)


if __name__ == '__main__':
    print(sum_1_112_3(1, 115, 3))
