"""
Функция check_substr.

Принимает две строки.
Если меньшая по длине строка содержится в большей, то возвращает True,
иначе False.
Если строки равны, то False.
Если одна из строк пустая, то True.
"""


def check_substr(s1, s2):
    s1 = str(s1)
    a1 = len(s1)
    s2 = str(s2)
    a2 = len(s2)
    if a1 == a2:
        return False
    if a1 == "" or a2 == "":
        return True
    if s1 in s2 or s2 in s1:
        return True
    else:
        return False


if __name__ == '__main__':
    check_substr("47", "564738")