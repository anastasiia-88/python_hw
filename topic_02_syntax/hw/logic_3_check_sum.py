"""
Функция check_sum.

Принимает 3 числа.
Вернуть True, если можно взять какие-то два из них и в сумме получить третье, иначе False
"""


def check_sum(a, b, c):
    if b + a == c or c + a == b or b + c == a:
        return True
    else:
        return False


if __name__ == '__main__':
    result = check_sum(99, 0, 99)
    print(result)

