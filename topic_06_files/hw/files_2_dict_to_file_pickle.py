"""
Функция save_dict_to_file_pickle.

Принимает 2 аргумента: строка (название файла или полный путь к файлу), словарь (для сохранения).

Сохраняет список в файл. Загрузить словарь и проверить его на корректность.
"""

import pickle


def save_dict_to_file_pickle(file, dictionary):
    with open(file, 'wb') as pickle_file:
        pickle.dump(dictionary, pickle_file)


if __name__ == '__main__':
    dicti = {'Python': '.py', 'C++': '.cpp', 'Java': '.java'}
    save_dict_to_file_pickle("2.txt", dicti)
    with open("2.txt", "rb")as f:
        diction = pickle.load(f)
        print(diction)
    print(f'type: {type(dicti)}')
    print(f'equal: {dicti == diction}')
