def save_dict_to_file_classic(file: str, dictionary: dict):
    with open(file, 'w') as my_file:
        my_file.write(str(dictionary))


if __name__ == '__main__':
    dicti = {"О верю, верю, счастье есть!"
             "Еще и солнце не погасло."
             "Заря молитвенником красным"
             "Пророчит благостную весть."
             "О верю, верю, счастье есть."

             "Звени, звени, златая Русь,"
             "Волнуйся, неуемный ветер!"
             "Блажен, кто радостью отметил"
             "Твою пастушескую грусть."
             "Звени, звени, златая Русь."

             "Люблю я ропот буйных вод"
             "И на волне звезды сиянье."
             "Благословенное страданье,"
             "Благословляющий народ."
             "Люблю я ропот буйных вод."}
    save_dict_to_file_classic("3.txt", dicti)
    with open("3.txt", 'r') as f:
        print(f.read())
