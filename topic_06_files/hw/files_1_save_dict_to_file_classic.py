"""
Функция save_dict_to_file_classic.

Принимает 2 аргумента: строка (название файла или полный путь к файлу), словарь (для сохранения).

Сохраняет список в файл. Проверить, что записалось в файл.
"""


def save_dict_to_file_classic(file: str, dictionary: dict):
    with open(file, 'w') as my_file:
        my_file.write(str(dictionary))


if __name__ == '__main__':
    save_dict_to_file_classic("1.txt", {'Python': '.py',
                                        'C++': '.cpp',
                                        'Java': '.java'})
    with open("1.txt", 'r') as f:
        print(f.read())
