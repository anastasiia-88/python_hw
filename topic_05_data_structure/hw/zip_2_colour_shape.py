"""
Функция zip_colour_shape.

Принимает 2 аргумента: список с цветами(зеленый, красный) и кортеж с формами (квадрат, круг).

Возвращает список с парами значений из каждого аргумента.

Если вместо list передано что-то другое, то возвращать строку 'First arg must be list!'.
Если вместо tuple передано что-то другое, то возвращать строку 'Second arg must be tuple!'.

Если list пуст, то возвращать строку 'Empty list!'.
Если tuple пуст, то возвращать строку 'Empty tuple!'.

Если list и tuple различного размера, обрезаем до минимального (стандартный zip).
"""


def zip_colour_shape(list1: list, shape: tuple):
    if type(list1) != list:
        return 'First arg must be list!'
    if type(shape) != tuple:
        return 'Second arg must be tuple!'
    if len(list1) == 0:
        return 'Empty list!'
    if len(shape) == 0:
        return 'Empty tuple!'
    return list(zip(list1, shape))


