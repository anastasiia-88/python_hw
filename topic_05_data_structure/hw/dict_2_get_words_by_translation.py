"""
Функция get_words_by_translation.

Принимает 2 аргумента:
ru-eng словарь содержащий {ru_word: [eng_1, eng_2 ...], ...}
слово для поиска в словаре (eng).

Возвращает список всех вариантов переводов (ru), если такое слово есть в словаре (eng),
если нет, то "Can't find English word: {word}".

Если вместо словаря передано что-то другое, то возвращать строку "Dictionary must be dict!".
Если вместо строки для поиска передано что-то другое, то возвращать строку "Word must be str!".

Если словарь пустой, то возвращать строку "Dictionary is empty!".
Если строка для поиска пустая, то возвращать строку "Word is empty!".
"""


def get_words_by_translation(rueng: dict, word: str):
    if type(rueng) != dict:
        return "Dictionary must be dict!"
    if type(word) != str:
        return "Word must be str!"
    if len(rueng) == 0:
        return "Dictionary is empty!"
    if len(word) == 0:
        return "Word is empty!"
    b = []
    for key, word1 in rueng.items():
        if word in word1:
            b.append(key)
    return b if len(b) > 0 else f"Can't find English word: {word}"