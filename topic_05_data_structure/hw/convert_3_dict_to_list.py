"""
Функция dict_to_list.

Принимает 1 аргумент: словарь.

Возвращает список: [
список ключей,
список значений,
количество уникальных элементов в списке ключей,
количество уникальных элементов в списке значений
].

Если вместо словаря передано что-то другое, то возвращать строку 'Must be dict!'.

Если dict пуст, то возвращать ([], [], 0, 0).
"""


def dict_to_list(diction: dict):
    if type(diction) != dict:
        return 'Must be dict!'
    if len(diction) == 0:
        return [], [], 0, 0
    key_list = list(diction.keys())
    value_list = list(diction.values())
    keys = len(set(key_list))
    values = len(set(value_list))
    return key_list, value_list, keys, values
