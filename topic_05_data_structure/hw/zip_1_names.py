"""
Функция zip_names.

Принимает 2 аргумента: список с именами и множество с фамилиями.

Возвращает список с парами значений из каждого аргумента.

Если вместо list передано что-то другое, то возвращать строку 'First arg must be list!'.
Если вместо set передано что-то другое, то возвращать строку 'Second arg must be set!'.

Если list пуст, то возвращать строку c.
Если set пуст, то возвращать строку 'Empty set!'.

Если list и set различного размера, обрезаем до минимального (стандартный zip).
"""


def zip_names(list1: list, names: set):
    if type(list1) != list:
        return 'First arg must be list!'
    if type(names) != set:
        return 'Second arg must be set!'
    if len(list1) == 0:
        return 'Empty list!'
    if len(names) == 0:
        return 'Empty set!'
    return list(zip(list1, names))


if __name__ == '__main__':
    result = zip_names(['Rose'], {'Black', 'Pink'})
    print(result)