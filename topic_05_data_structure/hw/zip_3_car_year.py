"""
Функция zip_car_year.

Принимает 2 аргумента: список с машинами и список с годами производства.

Возвращает список с парами значений из каждого аргумента, если один список больше другого,
то заполнить недостающие элементы строкой "???".

Подсказка: zip_longest.

Если вместо списков передано что-то другое, то возвращать строку 'Must be list!'.
Если список (хотя бы один) пуст, то возвращать строку 'Empty list!'.
"""
from itertools import zip_longest


def zip_car_year(cars: list, years: list):
    if type(cars) != list:
        return "Must be list!"
    if type(years) != list:
        return "Must be list!"
    if len(cars) == 0 or len(years) == 0:
        return 'Empty list!'
    return list(zip_longest(cars, years, fillvalue='???'))


if __name__ == '__main__':
    result = zip_car_year(['Kia'], ['2000', '2010'])
    print(result)
