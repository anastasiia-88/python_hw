"""
Функция list_to_str.

Принимает 2 аргумента: список и разделитель (строка).

Возвращает (строку полученную разделением элементов списка разделителем,
количество разделителей в получившейся строке в квадрате).

Если вместо списка передано что-то другое, то возвращать строку 'First arg must be list!'.

Если разделитель не является строкой, то возвращать строку 'Second arg must be str!'.

Если список пуст, то возвращать пустой tuple().

ВНИМАНИЕ: в списке могут быть элементы любого типа (нужно конвертировать в строку).
"""


def list_to_str(list1: list, a: str):
    if type(list1) != list:
        return 'First arg must be list!'
    if type(a) != str:
        return 'Second arg must be str!'
    if len(list1) == 0:
        return tuple()
    string = []
    for b in list1:
        string.append(str(b))
    str1 = a.join(string)
    return str1, str1.count(a) ** 2


if __name__ == '__main__':
    list_to_str([1, 2, 3, 4], '!')
