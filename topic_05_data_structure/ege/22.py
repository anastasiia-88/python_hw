for i in range(300):
    x = i
    K = 9
    L = 0
    while x >= K:
        L = L + 1
        x = x - K
    M = x
    if M < L:
        M = L
        L = x
    if L == 5 and M == 8:
        print(i)


print("---------------------------------------------------")

for i in range(300):
    x = i
    S = x
    R = 0
    while x > 0:
       d = x % 2
       R = 10*R + d
       x = x // 2
    S = R + S
    if S >= 10000:
        print(i)
print("------------------------------")
for i in range(100000):
    x = i
    L = x
    M = 65
    if L % 2 == 0:
        M = 52
    while L != M:
        if L > M:
            L = L - M
        else:
            M = M - L
    if M == 26:
        print(i)