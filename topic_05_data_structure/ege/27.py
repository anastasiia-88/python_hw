f = open("27.A")
n = int(f.readline())
max_sum =0
for i in range (n):
    a,b = map(int,f.readline().split())
    max_sum += max(a,b)
if max_sum % 4 != 0:
    print(max_sum)
