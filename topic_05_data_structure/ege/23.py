def numprog(start, x):
    if x < start:
        return 0
    if x == start:
        return 1
    K = numprog(start, x - 2)
    if x % 2 == 0:
        K += (numprog(start, x//2))
    return K


print(numprog(2,40))
